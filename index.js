const electron = require('electron');
const url = require('url');
const path = require('path');


//process.env.NODE_ENV = 'production';

const {app, BrowserWindow, Menu, ipcMain, globalShortcut} = electron;

let mainWindow;
let etcWindow;


app.on('ready', () => {
    //Create Window
    mainWindow = new BrowserWindow({
        frame: false,
        width: 1377,
        height: 818,
        webPreferences: {
            nodeIntegration: true
        },
        backgroundColor: '#333333',
        icon: __dirname + '/assets/icons/win/icon.ico'
    });

    //load html file

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'main.html'),
        protocol: 'file',
        slashes: true
    }));
    mainWindow.on('closed', () => {
        app.quit();
    });

    // Build menu from template
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);

    // insert menu
    Menu.setApplicationMenu(mainMenu);


    //shortcuts


});


function openETCWindow(file, width, height) {
    if (etcWindow !== null) etcWindow = null;
    if (width === undefined || height === undefined) {
        etcWindow = new BrowserWindow({
            frame: false,
            width: 550,
            height: 466,
            webPreferences: {
                nodeIntegration: true
            },
            backgroundColor: '#333333',
            icon: __dirname + '/assets/icons/win/icon.ico',
            resizable: false
        });
        etcWindow.loadURL(url.format({
            pathname: path.join(__dirname, file),
            protocol: 'file',
            slashes: true
        }));
    } else {
        etcWindow = new BrowserWindow({
            frame: false,
            width: width,
            height: height,
            webPreferences: {
                nodeIntegration: true
            },
            backgroundColor: '#333333',
            icon: __dirname + '/assets/icons/win/icon.ico',
            resizable: false
        });
        etcWindow.loadURL(url.format({
            pathname: path.join(__dirname, file),
            protocol: 'file',
            slashes: true
        }));
    }
}


//recive data:
ipcMain.on('NAV', (event, item) => {
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, item),
        protocol: 'file',
        slashes: true
    }));
});
ipcMain.on('TOMAIN', (event, item) => {
    mainWindow.webContents.send('TOMAIN', item);
});


ipcMain.on('SUB:NAV', (event, item) => {
    if (item === '-CLOSE-') {
        if (etcWindow !== undefined && etcWindow !== null) {
            etcWindow.close();
            etcWindow = null;
        }
    } else {
        var json = JSON.parse(item);
        if (json.HEIGHT === undefined || json.WIDTH === undefined) {
            if (etcWindow !== undefined && etcWindow !== null) {
                etcWindow.close();
                etcWindow = null;
            }
            openETCWindow(json.SITE);
        } else {
            if (etcWindow !== undefined && etcWindow !== null) {
                etcWindow.close();
                etcWindow = null;
            }
            openETCWindow(json.SITE, parseInt(json.WIDTH), parseInt(json.HEIGHT));
        }

    }
});


//create menu tamplate
const mainMenuTemplate = [
    {
        label: 'File',
        submenu: [
            {
                label: 'Quit',
                accelerator: process.platform === 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                click() {
                    app.quit();
                }
            }
        ]
    }
];

if (process.platform === 'darwin') {
    mainMenuTemplate.unshift({});
}

if (process.env.NODE_ENV !== 'production') {
mainMenuTemplate.push(
    {
        label: 'Dev Tools',
        submenu: [
            {
                label: 'Toggle Developer Tools',
                accelerator: 'F12',
                click: (item, focusedWindow) => {
                    focusedWindow.toggleDevTools();
                }
            }
        ]
    });
}



