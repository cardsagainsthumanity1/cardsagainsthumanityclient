$(document).ready(() => {
    updateLoading();
});

var step = 0;

function updateLoading() {
    if (step === 0) {
        $(".loading-text").html("Loading");
        step = 1;
        setTimeout(updateLoading, 1000);
    } else if (step === 1) {
        $(".loading-text").html("Loading.");
        step = 2;
        setTimeout(updateLoading, 1000);
    } else if (step === 2) {
        $(".loading-text").html("Loading..");
        step = 3;
        setTimeout(updateLoading, 1000);
    } else if (step === 3) {
        $(".loading-text").html("Loading...");
        step = 0;
        setTimeout(updateLoading, 1000);
    }
}

function setCookie(cname, cvalue, exdays,) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/;Secure";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function issetCookie(cookie) {
    var name = getCookie(cookie);
    if (name !== "") {
        return true
    } else {
        return false;
    }
}

function deleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

function deleteCookie(name) {
    document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/;Secure"
}


function load(ID, PFAD) {
    $("#" + ID).load(PFAD);
}

function setAttribute(ID, ATTRIBUT, TOSET) {
    try {
        document.getElementById(ID).setAttribute(ATTRIBUT, TOSET);
    } catch (e) {
        if (issetCookie("DEBUG")) console.error(e);
    }

}

function delAttribute(ID, ATTRIBUT) {
    document.getElementById(ID).removeAttribute(ATTRIBUT);
}

function getAttribute(ID, ATTRIBUT) {
    return document.getElementById(ID).getAttribute(ATTRIBUT);
}

function setHTML(ID, TEXT) {
    document.getElementById(ID).innerHTML = TEXT;
}

function getHTML(ID) {
    return document.getElementById(ID).innerHTML;
}

function getbyId(ID) {
    return document.getElementById(ID);
}

function getHTMLbyClassID(CLASS, ID) {
    var x = document.getElementsByClassName(CLASS);
    return x[ID].innerHTML;
}

function getElementsbyClass(CLASS) {
    var x = document.getElementsByClassName(CLASS);
    return x;
}

function getValue(ID) {
    return document.getElementById(ID).value;
}

function setValue(ID, VALUE) {
    document.getElementById(ID).value = VALUE;
}

function getURL() {
    return window.location.href;
}


//for delete tags
Element.prototype.remove = function () {
    this.parentElement.removeChild(this);
};
NodeList.prototype.remove = HTMLCollection.prototype.remove = function () {
    for (var i = this.length - 1; i >= 0; i--) {
        if (this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
};

NodeList.prototype.setClass = HTMLCollection.prototype.setClass = function (cname) {
    for (var i = this.length - 1; i >= 0; i--) {
        if (this[i] && this[i].parentElement) {
            this[i].className = cname;
        }
    }
};


String.prototype.includes = function (str) {
    var returnValue = false;

    if (this.indexOf(str) !== -1) {
        returnValue = true;
    }

    return returnValue;
};

function replaceSpecial(str) {
    return str.replace(/[&\/\\#, +()$~%.'":*?<>{}]/g, '_');
}

function httpGet(theUrl) {
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.open("GET", theUrl, false);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            return xmlhttp.responseText;
        }
    };
}

function copyToClipboard(str) {
    // Temporäres Element erzeugen
    var el = document.createElement('textarea');
    // Den zu kopierenden String dem Element zuweisen
    el.value = str;
    // Element nicht editierbar setzen und aus dem Fenster schieben
    el.setAttribute('readonly', '');
    el.style = {position: 'absolute', left: '-9999px'};
    document.body.appendChild(el);
    // Text innerhalb des Elements auswählen
    el.select();
    // Ausgewählten Text in die Zwischenablage kopieren
    document.execCommand('copy');
    // Temporäres Element löschen
    document.body.removeChild(el);
}

String.prototype.copyToClipboard = function (str) {
    copyToClipboard(str);
};


function isChecked(id) {
    return document.getElementById(id).checked;
}

function GetAgentToken(length) {
    return randString(length);
}

function randString(length) {
    if (length === undefined) {
        return 'xxxxxxxxxxxxxxxxxxxx'.replace(/x/g, function (c) {
            return ((Math.random() * 16) | 0).toString(16)
        });
    } else {
        var str = "";
        for (var i = 0; i < length; i++) {
            str = str + "x";
        }
        return str.replace(/x/g, function (c) {
            return ((Math.random() * 16) | 0).toString(16)
        });
    }

}


function arrayShuffle() {
    var tmp, rand;
    for (var i = 0; i < this.length; i++) {
        rand = Math.floor(Math.random() * this.length);
        tmp = this[i];
        this[i] = this[rand];
        this[rand] = tmp;
    }
}

Array.prototype.shuffle = arrayShuffle;
Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function getIndex(array, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] === value) {
            return i;
        }
    }
}


function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function searchInTable(tableID, inputID, col) {
    // Declare variables
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById(inputID);
    filter = input.value.toUpperCase();
    table = document.getElementById(tableID);
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[col];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function syntaxHighlight(json) {
    if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}


function elementExists(id) {
    return document.getElementById(id)!==undefined;
}
