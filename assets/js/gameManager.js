var players = {};
var currentQCard;
var playedACards = [];
var dumpACards = [];

var cardCountDec = 0;

var serverSettings = {};

const niceModal = new modal("modal");

$(document).ready(() => {
    connectionInfo = {};

    if (process.env.NODE_ENV === 'production') {
    }


    $(".title").html("» Connect to Server «")
    setLogin();

});

function setLogin(alert) {
    var redText = "";

    if (alert !== undefined) {
        redText = redText + "<br><small style='color: #ff3f3f'>" + alert + "</small>";
    }

    $("#content").html("<div class=\"c-bg container\" style=\"margin-top: 10em; padding: 1em;\">\n" +
        "\n" +
        "        <div id=\"form\">\n" +
        "            <div class=\"text-center\">\n" +
        "                <h2>Login</h2>" + redText +
        "            </div>\n" +
        "            <div class=\"form-group\">\n" +
        "                <label for=\"name\" class=\"bmd-label-floating\">Benutzername</label>\n" +
        "                <input type=\"text\" id=\"name\" value='' class=\"form-control\">\n" +
        "            </div>\n" +
        "            <div class=\"form-group\">\n" +
        "                <label for=\"pw\" class=\"bmd-label-floating\">Passwort (Optional)</label>\n" +
        "                <input type=\"password\" id=\"pw\" class=\"form-control\">\n" +
        "            </div>\n" +
        "            <div class=\"row\">\n" +
        "                <div class=\"form-group col\">\n" +
        "                    <label for=\"adress\" class=\"bmd-label-floating\">IP adresse</label>\n" +
        "                    <input type=\"text\" id=\"adress\" value='127.0.0.1' class=\"form-control\">\n" +
        "                </div>\n" +
        "                <div class=\"form-group col-3\">\n" +
        "                    <label for=\"port\" class=\"bmd-label-floating\">Port</label>\n" +
        "                    <input type=\"number\" min=\"0\" step=\"1\" value='8081' id=\"port\" class=\"form-control\">\n" +
        "                </div>\n" +
        "            </div>\n" +
        "\n" +
        "            <div class='row'>" +
        "              <div class='col'><a href=\"javascript:void(0)\" onclick=\"connect()\" class=\"btn btn-info\">Login</a></div>" +
        "              <div class='col-3'><div class=\"custom-control custom-checkbox\"><input type=\"checkbox\" id='saveLogin' class=\"custom-control-input\" checked> <label class=\"custom-control-label\" for=\"saveLogin\">Diesen Login Speichern</label></div></div>" +
        "</div>" +
        "        </div>\n" +
        "    </div>");
    discordRPData.DETAILS = "Sucht sich einen Benutzernamen aus ('Prinzessin der Klobürste' auch Cool.... )";
    discordRPData.STATUS = " » Login";
    initLastServers();


}


function connect(loginData) {
    aCards = [];
    usedBlankCards = 0;
    playedACards = [];

    if (loginData === undefined) {
        const name = $("#name").val();
        const adress = $("#adress").val();
        const port = $("#port").val();
        const pw = $("#pw").val();

        loginData = {};

        if (adress === "") {

            return;
        }
        if (port === "") {

            return;
        }
        if (name === "") {

            return;
        }
        var password = undefined;
        if (pw !== "") {
            password = pw;
        }
        loginData["NAME"] = "";
        loginData["USERNAME"] = name;
        loginData["IP"] = adress;
        loginData["PORT"] = parseInt(port);
        loginData["PASSWORD"] = password;
        loginData["SAVE"] = isChecked("saveLogin");
    }
    if (loginData.NAME!==undefined&&loginData.NAME!==""){
        $.snackbar({content: "Verbinde mit <i>"+loginData.NAME+"</i>...", htmlAllowed: true});
    }

    setHTML("content", "" +
        "<div class=\"c-bg container\" style=\"margin-top: 10em; padding: 1em;\">\n" +
        "  <div class=\"text-center text-white\" style='margin-bottom: 2em'>\n" +
        "      <i>Connecting...</i>\n" +
        "  </div>\n" +
        "  <div class=\"progress\">\n" +
        "      <div id=\"pBar\" class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"0\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n" +
        "  </div>\n" +
        "</div>");

    connectServer(false, loginData);


}

//called when disconnected
function interruptGame() {
    setState(GameState.DISCONNECTED);
}


function initGame() {
    $("#content").load("./subSites/game.html", () => {
        setState(GameState.WAIT_TO_START);
        //updatePlayers(true);
        $("#settingsCard").hide();
    });
    $(".title").html("» Cards Against Humanity «");
}

var GameState = {
    CHOOSE_CARDS: 1,
    VOTING: 2,
    END: 3,
    WAIT_TO_START: 4,
    DISCONNECTED: 5,
    LOGIN: 6
}

var currentState = 4;


function updatePlayers(bool) {
    if (bool !== undefined && bool) {
        client.write(JSON.stringify({ACTION: "GET_PLAYERS"}));
        return;
    }

    var html = "";

    if (currentState===GameState.WAIT_TO_START){
        discordRPData.DETAILS = "Wartet auf Spieler ("+Object.keys(players).length+"/"+serverSettings.MAX_PLAYERS+")";
        discordRPData.STATUS = "Wartet bis das Spiel beginnt...";
    }

    Object.entries(players).forEach(([name, data]) => {

        var before = "";
        var after = "";

        if (players[name].isWinner) {
            before = before + "<i style='font-size: 14px' class=\"fas fa-trophy\"></i>";
        }

        if (players[name].isCzar) {
            before = before + "<i style='font-size: 14px' class=\"fas fa-gavel\"></i>";
        }
        if (players[name].isAdmin) {
            before = before + "<i style='font-size: 14px' class=\"fas fa-star\"></i>";
        }

        if (players[name].isReady) {
            after = after + " <i style='font-size: 14px' class='material-icons'>done</i>";
        }
        if (!players[name].isConnected) {
            after = after + " <i style='font-size: 14px' class='material-icons'>wifi_off</i>";
        }
        if (players[name].isLoading) {
            after = after + " <i style='font-size: 14px' class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>";
        }

        html = html + "<li>" + before + " " + name + " <b>" + players[name].points + "</b>" + after + "</li>";
    });
    if (elementExists("players")) setHTML("players", html);

    if (players[myName].isAdmin && (currentState === GameState.END || currentState === GameState.WAIT_TO_START)) {
        $("#settingsCard").show();
    } else {
        $("#settingsCard").hide();
    }
}

var playedCard = {SLOT_1: undefined, SLOT_2: undefined, SLOT_3: undefined};

function playCard(id, slot, bool, text) {
    var qData = getQCard(currentQCard);
    //var text;
    if (slot === undefined) slot = 1;
    if (bool === undefined) {
        bool = false;
    }
    client.write(JSON.stringify({ACTION: "PLAY_CARD", ID: id, SLOT: slot, SW: bool, DATA: text}));

    for (var i = 1; i <= qData.ANSWER_COUNT; i++) {
        $("#" + id + "_PLAY_BTN_" + i).prop('disabled', true);
        setAttribute(id + "_PLAY_BTN_" + i, "onclick", "");
    }

    if (playedCard["SLOT_" + slot] !== undefined) {
        for (var i2 = 1; i2 <= qData.ANSWER_COUNT; i2++) {


            if (playedCard["SLOT_" + slot].SW !== undefined) {
                $("#" + playedCard["SLOT_" + slot].ID + "_PLAY_BTN_" + i2).prop('disabled', false);
                setAttribute(playedCard["SLOT_" + slot].ID + "_PLAY_BTN_" + i2, "onclick", "playCard(\"" + playedCard["SLOT_" + slot].ID + "\"," + i2 + ",true,\"" + playedCard["SLOT_" + slot].TEXT + "\");");
            } else {
                $("#" + playedCard["SLOT_" + slot] + "_PLAY_BTN_" + i2).prop('disabled', false);
                setAttribute(playedCard["SLOT_" + slot] + "_PLAY_BTN_" + i2, "onclick", "playCard(\"" + playedCard["SLOT_" + slot] + "\"," + i2 + ");");
            }
        }
    }
    if (bool) {
        playedCard["SLOT_" + slot] = {ID: id, SW: true, TEXT: text};
    } else {
        playedCard["SLOT_" + slot] = id;
    }

}

function showCard(id,json) {
    if (id===undefined){
        $("#" + json.ID + "_overlay").hide();
        $("#" + json.ID).show();
        global.cardsToVote[json.OWNER][json.SLOT].isShown = true;
        if (canVote()&&players[myName].isCzar){
            $(".cah-Vote-BTN").show();
        }
        return;
    }

    $("#" + id + "_overlay").hide();
    $("#" + id).show();
    if (players[myName].isCzar){
        console.log(id);

        var slot = getAttribute(id,"cah-slot");
        var owner = getAttribute(id,"cah-card-owner");
        console.log(global.cardsToVote);
        console.log(global.cardsToVote[owner]);
        console.log("SLOT_"+slot);
        console.log(global.cardsToVote[owner][slot]);
        global.cardsToVote[owner][slot].isShown = true;
        client.write(JSON.stringify({ACTION: "SHOW_CARD", ID: id, SLOT: slot, OWNER: owner}));
        if (canVote()){
            $(".cah-Vote-BTN").show();
        }
    }

}

function voteCard(name) {
    if (!players[myName].isCzar) return;
    if (!canVote()){
        $.snackbar({content: "Um zu Voten, müssen alle Karten aufgedeckt sein!", htmlAllowed: true});
        return;
    }
    client.write(JSON.stringify({ACTION: "VOTE", DATA: name}));
}

var usedBlankCards = 0;

function playSWCard(slot) {
    var text = $("#swCardText").val();
    var qData = getQCard(currentQCard);
    if (serverSettings.ALLOW_BLANK_CARDS && usedBlankCards < serverSettings.BLANK_CARDS) {
        if (text === undefined || text === "") {
            $.snackbar({content: "Bitte gib eine Antwort ein!", htmlAllowed: true});
            return;
        }
        usedBlankCards++;

        $("#swCardText").val("");
        var html = getHTML("aCards");
        var id = randString(64);
        if (qData.ANSWER_COUNT === 1) {
            html = "<div id='" + id + "' class='col cah-aCard' style='margin-left: 1em; margin-top: 1em'><div style=\"margin: 10px !important;\">" + text + "</div><button type=\"button\" id='" + id + "_PLAY_BTN_1' class=\"cah-Vote-BTN\" onclick='playCard(\"" + id + "\",1,true,\"" + text + "\")'>PLAY</button></div>" + html;
        } else {
            var temp = "";
            temp = temp + "<div id='" + id + "' class='col cah-aCard' style='margin-left: 1em; margin-top: 1em'><div style=\"margin: 10px !important;\">" + text + "</div><div class=\"row\" style='position: absolute; bottom: 0'>";
            for (var i2 = 1; i2 <= qData.ANSWER_COUNT; i2++) {
                temp = temp + "<div class='col'><button type=\"button\" id='" + id + "_PLAY_BTN_" + i2 + "' class=\"btn btn-light\" onclick='playCard(\"" + id + "\"," + i2 + ",true,\"" + text + "\")'>" + i2 + "</button></div>";
            }
            temp = temp + "</div></div>";
            html = temp + html;
        }
        setHTML("aCards", html);
        playCard(id, slot, true, text);
    } else {
        $.snackbar({content: "Du kannst deine Selbstgeschriebende Karte nicht legen!", htmlAllowed: true});
    }
}



function canVote() {
    var canVote = true;
    Object.entries(global.cardsToVote).forEach(([name, value]) => {
        Object.entries(global.cardsToVote[name]).forEach(([slot, value1]) => {
            if (!global.cardsToVote[name][slot].isShown){
                canVote = false;
                return canVote;
            }

        });
    });
    return canVote;
}

function openQCardInfo(id){
    var card = getQCard(id);
    if (card===undefined){
        $.snackbar({content:"Die Karte wurde nicht gefunden!"});
        return;
    }

    niceModal.close();
    niceModal.clear();
    niceModal.setTitle("Karten Info");
    niceModal.setContent("<div class='row' style='margin-bottom: 1em'><div class='col'>TAG: "+card.CTAG+"</div><div class='col'>Anzahl der Antworten: "+card.ANSWER_COUNT+"</div></div>" +
        "<h5>Text:</h5><div class='row' style='margin-bottom: 1em'><div class='col' style='padding: 5px; background-color: rgba(255,255,255,0.1); border: rgba(255,255,255,0.4) solid 1px'>"+card.TEXT+"</div></div>" +
        "<div style='margin-bottom: 1em' class='row'><div class='col'>Lizenz: "+card.LICENCE+"</div></div>" +
        "<div style='margin-bottom: 1em' class='row'><div class='col'>Website: "+card.WEBSITE+"</div></div>" +
        "<div style='margin-bottom: 1em' class='row'><div class='col'>Datei Name: "+card.FILE+"</div><div class='col'>ID: "+card.ID+"</div></div>");
    niceModal.setFooter("<button class='btn btn-secondary' onclick='niceModal.close()'>Schließen</button>");
    niceModal.build();
    niceModal.open();
}
function openACardInfo(id){
    var card = getACard(id,true);
    if (card===undefined){
        $.snackbar({content:"Die Karte wurde nicht gefunden!"});
        return;
    }

    niceModal.close();
    niceModal.clear();
    niceModal.setTitle("Karten Info");
    niceModal.setContent("<div style='margin-bottom: 1em' class='row'><div class='col'>TAG: "+card.CTAG+"</div></div>" +
        "<h5>Text:</h5><div style='margin-bottom: 1em' class='row'><div class='col' style='padding: 5px; background-color: rgba(255,255,255,0.1); border: rgba(255,255,255,0.4) solid 1px'>"+card.TEXT+"</div></div>" +
        "<div style='margin-bottom: 1em' class='row'><div class='col'>Lizenz: "+card.LICENCE+"</div></div>" +
        "<div style='margin-bottom: 1em' class='row'><div class='col'>Website: "+card.WEBSITE+"</div></div>" +
        "<div style='margin-bottom: 1em' class='row'><div class='col'>Datei Name: "+card.FILE+"</div><div class='col'>ID: "+card.ID+"</div></div>");
    niceModal.setFooter("<button class='btn btn-secondary' onclick='niceModal.close()'>Schließen</button>");
    niceModal.build();
    niceModal.open();
}

function setState(state, data) {
    $(".timer").html("-");
    if (state === 1) {
        //choose cards
        $("#settingsCard").hide();
        currentState = state;
        var card = "";
        var qData = getQCard(currentQCard);
        if (players[myName].isAdmin) {
            card = card + "<span><i style='font-size: 14px; right: 5px; position: absolute' onclick='dumpQCard()' class=\"fas fa-times pointer\"></i></span>";
        }

        card = card + qData.TEXT;
        card = card + "<span style='font-size: 14px; right: 5px; bottom: 5px; position: absolute' class='pointer' onclick='openQCardInfo(\""+qData.ID+"\")'>"+qData.CTAG+" <i class=\"fas fa-caret-right\"></i></span>";
        setHTML("qCard", card);
        cardCountDec=0;
        discordRPData.DETAILS = qData.TEXT.replace(/<\/?[^>]+(>|$)/g, "");
        if (players[myName].isCzar) {
            discordRPData.STATUS = "ist der CZAR!";
            setHTML("cardsContainer", "<div class=\"text-center text-white\" style=\"margin-top: 10em\">\n" +
                "    <h1 class=\"fas fa-gavel\"></h1>\n" +
                "<p>Du bist der Czar!</p>" +
                "</div>");
        } else {
            discordRPData.STATUS = "Wählt Karten aus...";
            var cards = "<div class=\"row\" style=\"margin-left: 1em\" id='aCards'>";
            if (aCards.length > 10) {
                for (var i = 0; i < 10; i++) {
                    cardCountDec++;
                    if (qData.ANSWER_COUNT === 1) {
                        cards = cards + "<div id='" + aCards[i].ID + "' class=\"col cah-aCard\" style=\"margin-left: 1em; margin-top: 1em\">\n";

                        cards = cards + "<span class='dumpABTN'><i style='font-size: 14px; right: 10px; top: 10px; position: absolute' onclick='dumpACard(\""+aCards[i].ID+"\")' class=\"fas fa-times pointer\"></i></span>";

                        cards = cards + "            <div style=\"margin: 10px !important;\">\n" +
                            aCards[i].TEXT +
                            "            </div>\n" +
                            "<span style='font-size: 14px; right: 5px; bottom: 25px; position: absolute' class='pointer' onclick='openACardInfo(\""+aCards[i].ID+"\")'>"+qData.CTAG+" <i class=\"fas fa-caret-right\"></i></span>"+
                            "            <button class=\"cah-Vote-BTN\" style='position: absolute;' id='" + aCards[i].ID + "_PLAY_BTN_1' onclick='playCard(\"" + aCards[i].ID + "\")'>PLAY</button>\n" +
                            "        </div>";

                    } else {
                        cards = cards + "<div id='" + aCards[i].ID + "' class=\"col cah-aCard\" style=\"margin-left: 1em; margin-top: 1em\">\n";
                        cards = cards + "<span class='dumpABTN'><i style='font-size: 14px; right: 10px; top: 10px; position: absolute' onclick='dumpACard(\""+aCards[i].ID+"\")' class=\"fas fa-times pointer\"></i></span>";
                        cards = cards + "            <div style=\"margin: 10px !important;\">\n" +
                            aCards[i].TEXT +
                            "            </div>\n" +
                        "<span style='font-size: 14px; right: 5px; bottom: 5px; position: absolute' class='pointer' onclick='openACardInfo(\""+aCards[i].ID+"\")'>"+qData.CTAG+" <i class=\"fas fa-caret-right\"></i></span>"+
                            "<div class=\"row\" style='position: absolute; bottom: 0'>\n";
                        for (var i2 = 1; i2 <= qData.ANSWER_COUNT; i2++) {
                            cards = cards + "<div class='col'><button type=\"button\" id='" + aCards[i].ID + "_PLAY_BTN_" + i2 + "' class=\"btn btn-light\" onclick='playCard(\"" + aCards[i].ID + "\"," + i2 + ")'>" + i2 + "</button></div>";
                        }

                        cards = cards + "</div>" +
                            "</div>";

                    }
                }
            } else {
                aCards.forEach(e => {
                    cardCountDec++;
                    if (qData.ANSWER_COUNT === 1) {
                        cards = cards + "<div id='" + e.ID + "' class=\"col cah-aCard\" style=\"margin-left: 1em; margin-top: 1em\">\n";
                        cards = cards + "<span class='dumpABTN'><i style='font-size: 14px; right: 10px; top: 10px; position: absolute' onclick='dumpACard(\""+e.ID+"\")' class=\"fas fa-times pointer\"></i></span>";
                        cards = cards + "            <div style=\"margin: 10px !important;\">\n" +
                            e.TEXT +
                            "            </div>\n" +
                            "<span style='font-size: 14px; right: 5px; bottom: 25px; position: absolute' class='pointer' onclick='openACardInfo(\""+e.ID+"\")'>"+qData.CTAG+" <i class=\"fas fa-caret-right\"></i></span>"+
                            "            <button id='" + e.ID + "_PLAY_BTN_1' class=\"cah-Vote-BTN\" onclick='playCard(\"" + e.ID + "\")'>PLAY</button>\n" +
                            "        </div>";

                    } else {
                        cards = cards + "<div id='" + e.ID + "' class=\"col cah-aCard\" style=\"margin-left: 1em; margin-top: 1em\">\n";
                        cards = cards + "<span class='dumpABTN'><i style='font-size: 14px; right: 10px; top: 10px; position: absolute' onclick='dumpACard(\""+e.ID+"\")' class=\"fas fa-times pointer\"></i></span>";
                        cards = cards + "            <div style=\"margin: 10px !important;\">\n" +
                            e.TEXT +
                            "            </div>" +
                            "<span style='font-size: 14px; right: 5px; bottom: 5px; position: absolute' class='pointer' onclick='openACardInfo(\""+e.ID+"\")'>"+qData.CTAG+" <i class=\"fas fa-caret-right\"></i></span>"+
                            "<div class=\"row\" style='position: absolute; bottom: 0'>\n";
                        for (var i2 = 1; i2 <= qData.ANSWER_COUNT; i2++) {
                            cards = cards + "<div class='col'><button type=\"button\" id='" + e.ID + "_PLAY_BTN_" + i2 + "' class=\"btn btn-light\" onclick='playCard(\"" + e.ID + "\"," + i2 + ")'>" + i2 + "</button></div>";
                        }

                        cards = cards + "</div>" +
                            "</div>";
                    }
                });
            }

            if (qData.ANSWER_COUNT === 1) {
                cards = cards + "<div class='col cah-aCard' style='margin-left: 1em; margin-top: 1em'><div style=\"margin: 10px !important;\"><textarea style='min-width: 280px; min-height: 220px; max-height: 220px; max-width: 280px; background-color: transparent; color: #ffffff' id='swCardText'></textarea></div><button class=\"cah-Vote-BTN\" onclick='playSWCard(1)'>PLAY</button></div>";
            } else {
                cards = cards + "<div class='col cah-aCard' style='margin-left: 1em; margin-top: 1em'><div style=\"margin: 10px !important;\"><textarea style='min-width: 280px; min-height: 220px; max-height: 220px; max-width: 280px; background-color: transparent; color: #ffffff' id='swCardText'></textarea></div><div class=\"row\" style='position: absolute; bottom: 0'>";
                for (var i2 = 1; i2 <= qData.ANSWER_COUNT; i2++) {
                    cards = cards + "<div class='col'><button type=\"button\" class=\"btn btn-light\" onclick='playSWCard(" + i2 + ")'>" + i2 + "</button></div>";
                }
                cards = cards + "</div></div>";
            }


            cards = cards + "</div>";
            setHTML("cardsContainer", cards);
        }


        $("#settingsCard").hide();
    } else if (state === 2) {
        //voting
        var qData = getQCard(currentQCard);
        $("#settingsCard").hide();
        //remove Cards

        Object.entries(playedCard).forEach(([slot, s]) => {
            if (playedCard[slot] !== undefined) {
                var card = getACard(playedCard[slot]);
                if (card !== undefined) {
                    playedACards.push(card);
                    aCards.remove(card);
                }
            }
        });
        discordRPData.DETAILS = qData.TEXT.replace(/<\/?[^>]+(>|$)/g, "");
        if (players[myName].isCzar){
            discordRPData.STATUS = "Deckt Karten auf...";
        }else{
            discordRPData.STATUS = "Wartet drauf zu gewinnen!";
        }


        currentState = state;
        if (data === undefined) return;
        var html = "<div class='row'>";
        var i = 0;
        global.cardsToVote = data;
        Object.entries(data).forEach(([name, data2]) => {
            if (i === 2) {
                i = 0;
                html = html + "</div><div class='row'>";
            }
            var border = false;
            if (Object.keys(data[name]).length>1){
                border = true;
            }

            html = html + "<div class=\"col\" style=\"margin-left: 1em; margin-top: 1em\"><div class='row'>";
            if (border){
                html = html + "<div style='border: #ffffff solid 1px; display: flex'>";
            }
            Object.entries(data2).forEach(([slot, data3]) => {
                global.cardsToVote[name][slot].ID2 = name + "_" + data[name][slot].ID + "_" + slot;

                html = html + "<div class=\"col cah-aCard\" cah-slot='"+slot+"' cah-card-owner='" + name + "' id='" + name + "_" + data[name][slot].ID + "_" + slot + "' style=\"margin: 1em; display: none\">\n" +
                    "            <div style=\"margin: 10px !important;\">\n" +
                    data[name][slot].TEXT +
                    "            </div>";
                if (players[myName].isCzar&&!border){
                    html = html + "<span style='font-size: 14px; right: 5px; bottom: 25px; position: absolute' class='pointer' onclick='openACardInfo(\""+data[name][slot].ID+"\")'>"+qData.CTAG+" <i class=\"fas fa-caret-right\"></i></span>";
                }else{
                    html = html + "<span style='font-size: 14px; right: 5px; bottom: 5px; position: absolute' class='pointer' onclick='openACardInfo(\""+data[name][slot].ID+"\")'>"+qData.CTAG+" <i class=\"fas fa-caret-right\"></i></span>";
                }
                if (players[myName].isCzar&&!border) html = html + "<button class=\"cah-Vote-BTN\" style='position: absolute; display: none' onclick='voteCard(\"" + name + "\")'>VOTE</button>\n";
                html = html + "</div>";

                if (players[myName].isCzar) {
                    html = html + "<div class=\"col cah-aCard pointer\" id='" + name + "_" + data[name][slot].ID + "_" + slot + "_overlay' onclick='showCard(\"" + name + "_" + data[name][slot].ID + "_" + slot + "\")' style=\"margin: 1em;\">";
                } else {
                    html = html + "<div class=\"col cah-aCard\" id='" + name + "_" + data[name][slot].ID + "_" + slot + "_overlay' style=\"margin: 1em;\">";
                }
                html = html + "<div style=\"margin: 10px !important;\" class='text-center'>";
                if (players[myName].isCzar) {
                    html = html + "Aufdecken";
                } else {
                    html = html + "Czar deckt auf";
                }

                html = html + "</div>\n";
                html = html + "</div>";

                i++;
            });
            if (border){
                if (players[myName].isCzar) html = html + "<button class=\"cah-Vote-BTN\" style='display: none' onclick='voteCard(\"" + name + "\")'>VOTE</button>\n";
                    html = html + "</div>";
            }
            html = html + "</div></div>";
        });

        html = html + "</div>";
        setHTML("cardsContainer", html);

    } else if (state === 3) {
        discordRPData.DETAILS = data+" hat gewonnen!";
        discordRPData.STATUS = "Runden Ende.";
        //end

        //reset Cards
        usedBlankCards = 0;
        if (playedACards.length > 0) {
            playedACards.forEach(card => {
                if (card)
                    aCards.push(card);
            });
            playedACards = [];
        }
        if (dumpACards.length > 0) {
            dumpACards.forEach(card => {
                if (card)
                    aCards.push(card);
            });
            dumpACards = [];
        }
        aCards.shuffle();


        currentState = state;
        if (serverSettings.NEW_GAME_AUTO) {
            setHTML("cardsContainer", "<div class=\"text-center text-white\" style=\"margin-top: 10em\">\n" +
                "    <h1 style='font-size: 22px' class=\"fas fa-trophy\"></h1>\n" +
                "<p><i>" + data + "</i> Hat gewonnen!<br> Die Nächste runde beginnt in <i class='timer'>-</i> Sekunden</p>" +
                "</div>");
            if (players[myName].isAdmin) {
                $("#settingsCard").show();
            } else {
                $("#settingsCard").hide();
            }
        } else {
            setHTML("cardsContainer", "<div class=\"text-center text-white\" style=\"margin-top: 10em\">\n" +
                "    <h1 style='font-size: 22px' class=\"fas fa-trophy\"></h1>\n" +
                "<p><i>" + data + "</i> Hat gewonnen!</p>" +
                "</div>");
        }
    } else if (state === 4) {
        //WAIT TO START
        discordRPData.DETAILS = "Wartet auf Spieler ("+Object.keys(players).length+"/"+serverSettings.MAX_PLAYERS+")";
        discordRPData.STATUS = "Wartet bis das Spiel beginnt...";
        currentState = state;
        setHTML("qCard", "Warte bis das Spiel beginnt...");
        setHTML("cardsContainer", "");
        if (players[myName].isAdmin) {
            $("#settingsCard").show();
        } else {
            $("#settingsCard").hide();
        }
    } else if (state === 5) {
        //disconnected
        discordRPData.DETAILS = "Hm.. Rage Quit?";
        discordRPData.STATUS = "Verbindung verloren";
        currentState = state;
        $("#content").load("./subSites/discon.html", () => {

        });
    } else if (state === 6) {
        //login
        currentState = state;
        setLogin("Der Server wurde geschlossen!")
    }
    if (players[myName].isLoading) {
        client.write(JSON.stringify({ACTION: "UPDATE_PLAYER_STATE"}))
    }

    if (currentState !== GameState.WAIT_TO_START && currentState !== GameState.END) {
        if (players[myName].isAdmin) {
            $("#stopBTN").show();
        } else {
            $("#stopBTN").hide();
        }
    } else {
        $("#stopBTN").hide();
    }

}

function dumpACard(id) {
    var card = getACard(id);

    if (cardCountDec<=4){
        $(".dumpABTN").hide();
    }
    if (cardCountDec<3){
        return;
    }
    cardCountDec--;


    if (card !== undefined) {
        dumpACards.push(card);
        aCards.remove(card);
        $("#" + id).hide();
        if (aCards < 10 && dumpACards > 0) {
            if (dumpACards.length > 0) {
                dumpACards.forEach(c => {
                    if (c)
                        aCards.push(c);
                });
                dumpACards = [];
            }
        }
    }

}

function startGame() {
    if (!players[myName].isAdmin) {
        $.snackbar({content: "Du hast dafür keine Rechte!", htmlAllowed: true});
        return;
    }
    if (Object.keys(players).length < 2) {
        $.snackbar({
            content: "Es sind zu wenig Spieler auf dem Server! es werden Mindestens 2 Benötigt",
            htmlAllowed: true
        });
        return;
    }

    client.write(JSON.stringify({ACTION: "START_GAME"}));
}

function dumpQCard() {
    if (!players[myName].isAdmin) {
        $.snackbar({content: "Du hast dafür keine Rechte!", htmlAllowed: true});
        return;
    }
    client.write(JSON.stringify({ACTION: "DUMP_QCARD"}));

}

function reconnect() {
    $("#reconBTN").prop('disabled', true);
    try {
        connectServer(true);
    } catch (e) {
        setLogin("Wiederverbindung fehlgeschlagen!");
        console.error(e);
    }
}


function connectServer(reconnect, loginData) {

    $("#recentServerBTN").hide();
    if (reconnect === undefined || !reconnect) {
        myName = loginData.USERNAME;
        connectionInfo = {host: loginData.IP, port: loginData.PORT}
    }


    client = new net.Socket();
    console.log("Connecting to Server...");
    client.connect(connectionInfo, () => {
        console.log("Connected!");
        if (reconnect) {
            client.write(JSON.stringify({ACTION: "RECONNECT", tmpID: players[myName].tmpID}));
        } else {

            client.write(JSON.stringify({NAME: loginData.USERNAME, PASSWORD: loginData.PASSWORD, ACTION: "LOGIN"}));
        }

    });


    client.on('data', function (data) {
        console.log('Received: ' + data);
        try {
            var json = JSON.parse(data);
            if (json.ACTION !== undefined) {
                if (json.ACTION === "LOGIN") {
                    if (json.TYPE === "OK") {
                        setAttribute("pBar", "aria-valuenow", "25");
                        setAttribute("pBar", "class", "progress-bar w-25");
                        if (loginData.SAVE) {
                            createLogin(json.SNAME, loginData.USERNAME, loginData.PASSWORD, loginData.IP, loginData.PORT);
                        }
                        if (loginData.ID!==undefined){
                            var l = getLogin(loginData.ID);
                            if (l!==undefined){
                                updateLogin(l,"NAME",json.SNAME);
                            }
                        }

                        client.write(JSON.stringify({ACTION: "GET_CARDS", CTYPE: "Q"}))
                    } else {
                        setAttribute("pBar", "aria-valuenow", "100");
                        setAttribute("pBar", "class", "progress-bar w-100");
                        console.error("Login failed! " + json.DATA);
                        setTimeout(() => {
                            if (json.DATA === "NAME_IN_USE") {
                                setLogin("Dieser name ist bereits in verwendung!");
                            } else {
                                setLogin();
                            }

                        }, 500);
                    }
                } else if (json.ACTION === "GET_CARDS") {
                    if (json.TYPE !== undefined && json.TYPE === "ERR") {
                        console.error("Failed to load Cards: " + data);
                        return;
                    }
                    if (json.CTYPE !== undefined) {
                        if (json.CTYPE === "Q") {
                            var max = json.MAX;
                            var state = json.STATE;
                            var cards = json.CARDS;
                            cards.forEach(c => {
                                qCards.push(c);
                            });
                            if ((max - 1) === state) {
                                setAttribute("pBar", "aria-valuenow", "50");
                                setAttribute("pBar", "class", "progress-bar w-50");
                                setTimeout(() => {
                                    client.write(JSON.stringify({ACTION: "GET_CARDS", CTYPE: "A"}));
                                }, 500);
                            } else {
                                client.write(JSON.stringify({ACTION: "GET_CARDS", CTYPE: "Q", STATE: state + 1}));
                            }
                        } else {


                            var max = json.MAX;
                            var state = json.STATE;
                            var cards = json.CARDS;
                            cards.forEach(c => {
                                aCards.push(c);
                            });
                            if ((max - 1) === state) {
                                setAttribute("pBar", "aria-valuenow", "75");
                                setAttribute("pBar", "class", "progress-bar w-75");
                                aCards.shuffle();
                                client.write(JSON.stringify({ACTION: "GET_SETTINGS", INIT: true}));
                            } else {
                                client.write(JSON.stringify({ACTION: "GET_CARDS", CTYPE: "A", STATE: state + 1}));
                            }
                        }
                    } else {
                        client.end();
                        client.destroy();
                        setLogin();
                    }
                } else if (json.ACTION === "GET_SETTINGS") {
                    var tmp = json;
                    delete tmp["ACTION"];
                    if (tmp.INIT) {
                        setAttribute("pBar", "aria-valuenow", "100");
                        setAttribute("pBar", "class", "progress-bar w-100");
                        delete tmp["INIT"];
                        serverSettings = tmp;
                        setTimeout(initGame, 1000);
                        return;
                    }
                    delete tmp["INIT"];
                    serverSettings = tmp;
                } else if (json.ACTION === "UPDATE_PLAYERS") {
                    var temp = json;
                    delete json["ACTION"];
                    players = temp;
                    updatePlayers();
                } else if (json.ACTION === "ALERT") {
                    $.snackbar({content: json.DATA, htmlAllowed: true});
                } else if (json.ACTION === "SET_STATE") {
                    if (parseInt(json.STATE) === 1) {
                        currentQCard = json.QCARD;
                    }
                    setState(parseInt(json.STATE), json.DATA);
                } else if (json.ACTION === "SET_TIMER") {
                    $(".timer").html(json.DATA);
                } else if (json.ACTION === "SHOW_CARD") {
                    if (!players[myName].isCzar) showCard(undefined,json);
                } else if (json.ACTION === "RECONNECT") {
                    if (json.DATA === "OK") {
                        players = json.PLAYERS;
                        $("#content").load("./subSites/game.html", () => {
                            setState(parseInt(json.STATE));
                            updatePlayers();
                        });
                        return;
                    }
                    setLogin("Wiederverbindung fehlgeschlagen!");
                }
            }
        } catch (e) {
            console.error(e);
        }
    });

    var canReconnect = true;

    client.on('close', function () {
        console.log('Connection closed');
        client.destroy();
        if (canReconnect) interruptGame();
        canReconnect = true;
    });
    client.on('error', function (err) {
        canReconnect = false;
        console.log('Connection closed');
        console.log(err.code);
        if (err.code === "ECONNREFUSED") {
            console.error("Connection refused! Please check the IP.");
            if (!client.destroyed) client.destroy();
            setLogin("Konnt nicht mit dem Server vberbinden! Bitte überprüfe die Server Adresse!");
            return;
        }
        if (err.code === "ECONNRESET") {
            console.error("Connection refused! Please check the IP.");
            if (!client.destroyed) client.destroy();
            interruptGame();
            return;
        }

        client.destroy();
        setState(GameState.LOGIN);
    });
    client.on('timeout', function () {
        console.log('Connection closed');
        client.destroy();
        interruptGame();
    });
}


function getQCard(id) {
    var res = undefined;
    qCards.forEach(e => {
        if (e.ID === id) {
            res = e;
            return res;
        }
    });
    return res;
}

function getACard(id, allDecks) {
    var res = undefined;
    aCards.forEach(e => {
        if (e.ID === id) {
            res = e;
            return res;
        }
    });
    if (allDecks===undefined||!allDecks){
        return res;
    }
    dumpACards.forEach(e => {
        if (e.ID === id) {
            res = e;
            return res;
        }
    });
    playedACards.forEach(e => {
        if (e.ID === id) {
            res = e;
            return res;
        }
    });
    return res;
}


function stopGame(isBTN) {
    if (!players[myName].isAdmin) {
        $.snackbar({content: "Du hast dafür keine Rechte!", htmlAllowed: true});
        return;
    }

    if (isBTN === undefined || !isBTN) {
        client.write(JSON.stringify({ACTION: "STOP_GAME"}))
        return;
    }
    if (parseInt(getAttribute("stopBTN", "data-click-count")) >= 1) {
        setAttribute("stopBTN", "data-click-count", "0");
        setAttribute("stopBTN", "style", "text-decoration: none; color: #ffffff; display: none;");
        client.write(JSON.stringify({ACTION: "STOP_GAME"}));
    } else {
        setAttribute("stopBTN", "style", "text-decoration: none; color: #ff0000");
        setAttribute("stopBTN", "data-click-count", "1");
    }
}


function deleteLogin(id, openList) {
    var tmp = getLogin(id);

    if (tmp !== undefined) {
        logins.remove(tmp);
        saveLogins();
    }
    openLastServers();
}

function createLogin(sName, uName, pw, ip, port) {
    var tmp = {NAME: sName, IP: ip, PORT: port, USERNAME: uName, PASSWORD: pw}
    tmp.ID = genLoginId(logins);
    logins.push(tmp);
    saveLogins();
    return tmp.ID;
}

function updateLogin(login, what, to) {
    logins.remove(login);
    login[what] = to;
    logins.push(login);
    saveLogins();
}

function saveLogins() {
    fs.writeFileSync("./data/logins.json", JSON.stringify(JSON.stringify(logins), null, 2), "utf8");
}

function getLogin(id) {
    var res;
    logins.forEach(e => {
        if (e.ID !== undefined && e.ID === id) {
            res = e;
            return res;
        }
    });
    return res;
}

function genLoginId(arrayList) {
    var id = randString(16);
    while (loginIdExists(arrayList, id)) {
        id = randString(16);
    }
    return id;
}

function loginIdExists(arrayList, id) {
    var exists = false;
    if (arrayList === undefined) return false;
    arrayList.forEach(e => {
        if (e.ID !== undefined && e.ID === id) {
            exists = true;
            return exists;
        }
    });
    return exists;
}




//DISCORD RICH PRESENCE


var discordRPData = {
    STATUS: "Lädt...",
    DETAILS: "Lädt..."
}
const DiscordRPC = require('discord-rpc');
// don't change the client id if you want this example to work
const clientId = '735550048624050196';

// only needed for discord allowing spectate, join, ask to join
DiscordRPC.register(clientId);

const rpc = new DiscordRPC.Client({ transport: 'ipc' });
const startTimestamp = new Date();

async function setActivity() {
    if (!rpc||!config.DISCORD_RICH_PRESENCE) {
        return;
    }
    var activity = {
        details: discordRPData.DETAILS,
        state: discordRPData.STATUS,
        startTimestamp,
        largeImageKey: 'icon',
        largeImageText: '--',
        instance: false
    };
    rpc.setActivity(activity);
}

rpc.on('ready', () => {
    if (!config.DISCORD_RICH_PRESENCE){
        return;
    }
    setActivity();

    // activity can only be set every 15 seconds
    setInterval(() => {
        setActivity();
    }, 15e3);
});

rpc.login({ clientId }).catch(console.error);


