/*
*
* BOOTSTRAP v4 VERSION
*
* */


class SideBuilder {

    constructor(id) {
        this.modules = [];
        this.footer = null;
        this.id = id;

        this.showCloseBTN = false;
        this.data = null;
        this.title = null;
        this.moduleType = {
            CUSTOM: -1,
            TEXTAREA: 1,
            HR: 2,
            SEPERATOR: 2,
            BUTTON: 3,
            TEXTFIELD: 4,
            PASSWORDFIELD: 5,
            EMAILFIELD: 6,
            BTNROW_3: 7,
            BTNROW_2: 8,
            TEXT: 9,
            CHECKBOX: 10,
            SELECT: 11,
            FILEINPUT: 12

        }
    }

    getID() {
        return this.id;
    }

    getModules() {
        return this.modules;
    }

    setData(data){
        this.data = data;
    }
    getData() {
        return this.data;
    }

    build() {
        var toBuild = "";
        if (this.title !== null && this.title !== undefined) {
            if (this.showCloseBTN) {
                toBuild = "<div class=\"sidebar--header\"><h3>" + this.title + "</h3> <a href=\"javascript:void(0)\" class=\"closebtn\" onclick=\"$('#" + this.id + "' ).removeClass('open');\">&times;</a></div>";
            } else {
                toBuild = "<div class=\"sidebar--header\"><h3>" + this.title + "</h3></div>";
            }

        } else {
            if (this.showCloseBTN) {
                toBuild = "<div class=\"sidebar--header\"><a href=\"javascript:void(0)\" class=\"closebtn\" onclick=\"$('#" + this.id + "' ).removeClass('open');\">&times;</a></div>";
            }
        }

        this.modules.forEach(e => {
            toBuild = toBuild + this.getModule(e);
        });

        if (this.footer !== undefined && this.footer !== null) {
            toBuild = toBuild + this.getFooterModule();
        } else {
            toBuild = toBuild + "<div style='padding-top: 2em;'></div>"
        }

        document.getElementById(this.id).innerHTML = toBuild;
    }

    materialDesign(){
        $('#'+this.id).bootstrapMaterialDesign();
    }

    open() {
        $("#" + this.id).addClass("open");
    }

    close() {
        $("#" + this.id).removeClass("open");
    }

    clear() {
        this.modules = [];
        this.data = null;
    }

    setTitle(title) {
        this.title = title;
    }

    enableCloseBTN(bool) {
        this.showCloseBTN = bool;
    }

    // -1 - custom
    // 1 - TextField
    // 2 - hr
    // 3 - Button <type - Button/a>
    // 4 - TextField
    // 5 - PasswordField
    // 6 - EMail Field
    // 7 - BTN Row (3 BTNs) <type - Button/a>
    // 8 - BTN Row (2 BTNs) <type - Button/a>
    // 9 - Text
    // 10 - Checkbox
    // 11 - Select
    // 12 - FileInput

    addModule(json) {
        this.modules.push(json);
    }

    setFooter(json) {
        this.footer = json;
    }

    // -1 - Custom
    // 0 - diable
    // 1 - BTN
    // 2 - BTN 2
    // 3 - BTN 3
    getFooterModule() {
        var res = "<div class='sidebar--footer'><div class='item'>";
        var json = this.footer;
        try {
        } catch (e) {
            return res + "<h4>Oops..</h4><p>Something went wrong...</p><small>" + e + "</small><br><br><small>JSON: " + json + "</small>" + "</div></div>"
        }
        if (json.moduleID === -1) {
            res = res + json.data;
        } else if (json.moduleID === 0) {
            return "";
        } else if (json.moduleID === 1) {
            if (json.type === "button") {
                if (json.options === undefined) {
                    res = res + "<button>" + json.value + "</button>";
                } else {
                    res = res + "<button " + json.options + ">" + json.value + "</button>";
                }
            } else {
                if (json.options === undefined) {
                    res = res + "<a>" + json.value + "</a>";
                } else {
                    res = res + "<a " + json.options + ">" + json.value + "</a>";
                }
            }
        } else if (json.moduleID === 2) {
            if (json.type === "button") {
                if (json.options === undefined) {
                    res = res + "<button>" + json.value + "</button>";
                } else {
                    res = res + "<button " + json.options + ">" + json.value + "</button>";
                }
                if (json.option2 === undefined) {
                    res = res + "<button>" + json.value2 + "</button>";
                } else {
                    res = res + "<button " + json.options2 + ">" + json.value2 + "</button>";
                }
            } else {
                if (json.options === undefined) {
                    res = res + "<a>" + json.value + "</a>";
                } else {
                    res = res + "<a " + json.options + ">" + json.value + "</a>";
                }
                if (json.options2 === undefined) {
                    res = res + "<a>" + json.value2 + "</a>";
                } else {
                    res = res + "<a " + json.options2 + ">" + json.value2 + "</a>";
                }
            }
        } else if (json.moduleID === 3) {
            if (json.type === "button") {
                if (json.options === undefined) {
                    res = res + "<button>" + json.value + "</button>";
                } else {
                    res = res + "<button " + json.options + ">" + json.value + "</button>";
                }
                if (json.option2 === undefined) {
                    res = res + "<button>" + json.value2 + "</button>";
                } else {
                    res = res + "<button " + json.options2 + ">" + json.value2 + "</button>";
                }
                if (json.options3 === undefined) {
                    res = res + "<button>" + json.value3 + "</button>";
                } else {
                    res = res + "<button " + json.options3 + ">" + json.value3 + "</button>";
                }
            } else {
                if (json.options === undefined) {
                    res = res + "<a>" + json.value + "</a>";
                } else {
                    res = res + "<a " + json.options + ">" + json.value + "</a>";
                }
                if (json.options2 === undefined) {
                    res = res + "<a>" + json.value2 + "</a>";
                } else {
                    res = res + "<a " + json.options2 + ">" + json.value2 + "</a>";
                }
                if (json.options3 === undefined) {
                    res = res + "<a>" + json.value3 + "</a>";
                } else {
                    res = res + "<a " + json.options3 + ">" + json.value3 + "</a>";
                }
            }
        } else {
            res = res + "<h4>Oops..</h4><p>Something went wrong...</p><small>This Module wit id <i>" + json.moduleID + " does not exist</i></small><br><br><small>JSON: " + json + "</small>"
        }


        res = res + "</div></div>";
        return res;
    }

    getModule(json) {
        try {

        } catch (e) {
            return "<div class='sidebar--item'><h4>Oops..</h4><p>Something went wrong...</p><small>" + e + "</small><br><br><small>JSON: " + json + "</small></div>"
        }

        var res = "";
        if (json.moduleID === -1) {
            return "<div class='sidebar--item'>" + json.custom + "</div>";
        } else if (json.moduleID === 1) {

            res = "<div class=\"sidebar--item\">";
            if (json.title !== undefined) {
                res = res + "<h4>" + json.title + "</h4>";
            }
            if (json.subTitle !== undefined) {
                res = res + "<p>" + json.subTitle + "</p>";
            }
            if (json.options !== undefined) {
                res = res + "<div class=\"sidebar--form\"><textarea " + json.options + "></textarea></div>";
            } else {
                res = res + "<div class=\"sidebar--form\"><textarea></textarea></div>";
            }
            if (json.small !== undefined) {
                res = res + "<small>" + json.small + "</small>";
            }
            return res + "</div>";
        } else if (json.moduleID === 2) {
            return "<div class='sidebar--item'><hr></div>";
        } else if (json.moduleID === 3) {
            if (json.type === "button") {
                res = "<div class=\"sidebar--item\">";
                if (json.title !== undefined) {
                    res = res + "<h4>" + json.title + "</h4>";
                }
                if (json.subTitle !== undefined) {
                    res = res + "<p>" + json.subTitle + "</p>";
                }
                if (json.options !== undefined) {
                    res = res + "<div class=\"sidebar--form\"><button class='btn btn-outline-primary' " + json.options + ">" + json.value + "</button></div>";
                } else {
                    res = res + "<div class=\"sidebar--form\"><button class='btn btn-outline-primary'>" + json.value + "</button></div>";
                }
                if (json.small !== undefined) {
                    res = res + "<small>" + json.small + "</small>";
                }
                return res + "</div>";
            } else {
                res = "<div class=\"sidebar--item\">";
                if (json.title !== undefined) {
                    res = res + "<h4>" + json.title + "</h4>";
                }
                if (json.subTitle !== undefined) {
                    res = res + "<p>" + json.subTitle + "</p>";
                }
                if (json.options !== undefined) {
                    res = res + "<div class=\"sidebar--form\"><a class='btn btn-outline-primary' " + json.options + ">" + json.value + "</a></div>";
                } else {
                    res = res + "<div class=\"sidebar--form\"><a class='btn btn-outline-primary'>" + json.value + "</a></div>";
                }
                if (json.small !== undefined) {
                    res = res + "<small>" + json.small + "</small>";
                }
                return res + "</div>";
            }
        } else if (json.moduleID === 4) {
            res = "<div class=\"sidebar--item\">";
            if (json.title !== undefined) {
                res = res + "<h4>" + json.title + "</h4>";
            }
            if (json.subTitle !== undefined) {
                res = res + "<p>" + json.subTitle + "</p>";
            }
            if (json.options !== undefined) {
                res = res + "<div class=\"sidebar--form form-group\"><input class=\"form-control\" type='text' " + json.options + "></div>";
            } else {
                res = res + "<div class=\"sidebar--form form-group\"><input class=\"form-control\" type='text'></div>";
            }
            if (json.small !== undefined) {
                res = res + "<small>" + json.small + "</small>";
            }
            return res + "</div>";
        } else if (json.moduleID === 5) {
            res = "<div class=\"sidebar--item\">";
            if (json.title !== undefined) {
                res = res + "<h4>" + json.title + "</h4>";
            }
            if (json.subTitle !== undefined) {
                res = res + "<p>" + json.subTitle + "</p>";
            }
            if (json.options !== undefined) {
                res = res + "<div class=\"sidebar--form form-group\"><input class=\"form-control\" type='password' " + json.options + "></div>";
            } else {
                res = res + "<div class=\"sidebar--form form-group\"><input class=\"form-control\" type='password'></div>";
            }
            if (json.small !== undefined) {
                res = res + "<small>" + json.small + "</small>";
            }
            return res + "</div>";
        } else if (json.moduleID === 6) {
            res = "<div class=\"sidebar--item\">";
            if (json.title !== undefined) {
                res = res + "<h4>" + json.title + "</h4>";
            }
            if (json.subTitle !== undefined) {
                res = res + "<p>" + json.subTitle + "</p>";
            }
            if (json.options !== undefined) {
                res = res + "<div class=\"sidebar--form form-group\"><input class=\"form-control\" type='email' " + json.options + "></div>";
            } else {
                res = res + "<div class=\"sidebar--form form-group\"><input class=\"form-control\" type='email'></div>";
            }
            if (json.small !== undefined) {
                res = res + "<small>" + json.small + "</small>";
            }
            return res + "</div>";
        } else if (json.moduleID === 7) {
            // 3 btn row
            if (json.type !== "button") {
                res = "<div class=\"sidebar--item\">";
                if (json.title !== undefined) {
                    res = res + "<h4>" + json.title + "</h4>";
                }
                if (json.subTitle !== undefined) {
                    res = res + "<p>" + json.subTitle + "</p>";
                }
                res = res + "<div class='sidebar--btnLine'>";
                if (json.options1 !== undefined) {
                    res = res + " <a class='btn btn-outline-primary' " + json.options1 + ">" + json.value1 + "</a>";
                } else {
                    res = res + " <a class='btn btn-outline-primary'>" + json.value1 + "</a>";
                }
                if (json.options2 !== undefined) {
                    res = res + " <a class='btn btn-outline-primary' " + json.options2 + ">" + json.value2 + "</a>";
                } else {
                    res = res + " <a class='btn btn-outline-primary'>" + json.value2 + "</a>";
                }
                if (json.options3 !== undefined) {
                    res = res + " <a class='btn btn-outline-primary' " + json.options3 + ">" + json.value3 + "</a>";
                } else {
                    res = res + " <a class='btn btn-outline-primary'>" + json.value3 + "</a>";
                }
                res = res + "</div>";
                if (json.small !== undefined) {
                    res = res + "<small>" + json.small + "</small>";
                }
                return res + "</div>";
            } else {
                res = "<div class=\"sidebar--item\">";
                if (json.title !== undefined) {
                    res = res + "<h4>" + json.title + "</h4>";
                }
                if (json.subTitle !== undefined) {
                    res = res + "<p>" + json.subTitle + "</p>";
                }
                res = res + "<div class='sidebar--btnLine'>";
                if (json.options1 !== undefined) {
                    res = res + " <button class='btn btn-outline-primary' " + json.options1 + ">" + json.value1 + "</button>";
                } else {
                    res = res + " <button class='btn btn-outline-primary'>" + json.value1 + "</button>";
                }
                if (json.options2 !== undefined) {
                    res = res + " <button class='btn btn-outline-primary' " + json.options2 + ">" + json.value2 + "</button>";
                } else {
                    res = res + " <button class='btn btn-outline-primary'>" + json.value2 + "</button>";
                }
                if (json.options3 !== undefined) {
                    res = res + " <button class='btn btn-outline-primary' " + json.options3 + ">" + json.value3 + "</button>";
                } else {
                    res = res + " <button class='btn btn-outline-primary'>" + json.value3 + "</button>";
                }
                res = res + "</div>";
                if (json.small !== undefined) {
                    res = res + "<small>" + json.small + "</small>";
                }
                return res + "</div>";
            }
        } else if (json.moduleID === 8) {
            // 2 btn row
            if (json.type !== "button") {
                res = "<div class=\"sidebar--item\">";
                if (json.title !== undefined) {
                    res = res + "<h4>" + json.title + "</h4>";
                }
                if (json.subTitle !== undefined) {
                    res = res + "<p>" + json.subTitle + "</p>";
                }
                res = res + "<div class='sidebar--btnLine'>";
                if (json.options1 !== undefined) {
                    res = res + " <a class='btn btn-outline-info' " + json.options1 + ">" + json.value1 + "</a>";
                } else {
                    res = res + " <a class='btn btn-outline-info'>" + json.value1 + "</a>";
                }
                if (json.options2 !== undefined) {
                    res = res + " <a class='btn btn-outline-info' " + json.options2 + ">" + json.value2 + "</a>";
                } else {
                    res = res + " <a class='btn btn-outline-info'>" + json.value2 + "</a>";
                }
                res = res + "</div>";
                if (json.small !== undefined) {
                    res = res + "<small>" + json.small + "</small>";
                }
                return res + "</div>";
            } else {
                res = "<div class=\"sidebar--item\">";
                if (json.title !== undefined) {
                    res = res + "<h4>" + json.title + "</h4>";
                }
                if (json.subTitle !== undefined) {
                    res = res + "<p>" + json.subTitle + "</p>";
                }
                res = res + "<div class='sidebar--btnLine'>";
                if (json.options1 !== undefined) {
                    res = res + " <button class='btn btn-outline-info' " + json.options1 + ">" + json.value1 + "</button>";
                } else {
                    res = res + " <button class='btn btn-outline-info'>" + json.value1 + "</button>";
                }
                if (json.options2 !== undefined) {
                    res = res + " <button class='btn btn-outline-info' " + json.options2 + ">" + json.value2 + "</button>";
                } else {
                    res = res + " <button class='btn btn-outline-info'>" + json.value2 + "</button>";
                }
                res = res + "</div>";
                if (json.small !== undefined) {
                    res = res + "<small>" + json.small + "</small>";
                }
                return res + "</div>";
            }
        } else if (json.moduleID === 9) {
            res = "<div class=\"sidebar--item\">";
            if (json.title !== undefined) {
                res = res + "<h4>" + json.title + "</h4>";
            }
            if (json.text !== undefined) {
                res = res + "<p>" + json.text + "</p>";
            }
            if (json.small !== undefined) {
                res = res + "<small>" + json.small + "</small>";
            }

            return res + "</div>";
        } else if (json.moduleID === 10) {
            res = "<div class=\"sidebar--item\">";
            if (json.title !== undefined) {
                res = res + "<h4>" + json.title + "</h4>";
            }
            if (json.subTitle !== undefined) {
                res = res + "<p>" + json.subTitle + "</p>";
            }
            if (json.id === undefined) {
                return "<h4>Oops..</h4><p>Something went wrong...</p><small>Please set the id of the checkbox!</small><br><br><small>JSON: " + json + "</small></div>";
            }
            if (json.options === undefined) {
                res = res + "<div class=\"sidebar--form\"><div class='switch'><label for='" + json.id + "'><input id='" + json.id + "' type='checkbox'>" + json.label + "</label></div></div>";
            } else {
                res = res + "<div class=\"sidebar--form\"><div class='switch'><label for='" + json.id + "'><input id='" + json.id + "' " + json.options + " type='checkbox'>" + json.label + "</label></div></div>";
            }

            if (json.small !== undefined) {
                res = res + "<small>" + json.small + "</small>";
            }

            return res + "</div>";
        } else if (json.moduleID === 11) {
            //select
            res = "<div class=\"sidebar--item\">";
            if (json.title !== undefined) {
                res = res + "<h4>" + json.title + "</h4>";
            }
            if (json.subTitle !== undefined) {
                res = res + "<p>" + json.subTitle + "</p>";
            }


            if (json.options === undefined) {
                res = res + "<div class=\"sidebar--form form-group\"><select class=\"form-control\">";
                JSON.parse(json.data.replace(/'/g, '"')).forEach(e => {
                    res = res + "<option value='" + e + "'>" + e + "</option>";
                });

                res = res + "</select></div>";
            } else {
                res = res + "<div class=\"sidebar--form form-group\"><select class=\"form-control\" id='" + json.id + "' " + json.options + ">";
                JSON.parse(json.data.replace(/'/g, '"')).forEach(e => {
                    res = res + "<option value='" + e + "'>" + e + "</option>";
                });

                res = res + "</select></div>";
            }

            if (json.small !== undefined) {
                res = res + "<small>" + json.small + "</small>";
            }

            return res + "</div>";

        } else if (json.moduleID === 12) {
            res = "<div class=\"sidebar--item\">";
            if (json.title !== undefined) {
                res = res + "<h4>" + json.title + "</h4>";
            }
            if (json.subTitle !== undefined) {
                res = res + "<p>" + json.subTitle + "</p>";
            }
            if (json.id === undefined) {
                return "<h4>Oops..</h4><p>Something went wrong...</p><small>Please set the id of the checkbox!</small><br><br><small>JSON: " + json + "</small></div>";
            }
            if (json.options === undefined) {
                if (json.label===undefined){
                    res = res + "<div class='sidebar--form custom-file'><input class='custom-file' id='"+json.id+"' type=\"file\"><label for='"+json.id+"' class=\"custom-file-label\"></label></div>";
                }else{
                    res = res + "<div class='sidebar--form custom-file'><input class='custom-file' id='"+json.id+"' type=\"file\"><label for='"+json.id+"' class=\"custom-file-label\">"+json.label+"</label></div>";
                }

            } else {
                if (json.label===undefined){
                    res = res + "<div class='sidebar--form custom-file'><input "+json.options+" id='"+json.id+"' class='custom-file' type=\"file\"><label for='"+json.id+"' class=\"custom-file-label\"></label></div>";
                }else{
                    res = res + "<div class='sidebar--form custom-file'><input "+json.options+" id='"+json.id+"' class='custom-file' type=\"file\"><label for='"+json.id+"' class=\"custom-file-label\">"+json.label+"</label></div>";
                }
            }


            if (json.small !== undefined) {
                res = res + "<small>" + json.small + "</small>";
            }

            return res + "</div>";
        }
    }
}