//modal script for bootstrap 4.3.1

class modal {

    constructor(modalID) {
        this.id = modalID;
        this.bool = false;
        this.content = null;
        this.header = null;
        this.footer = null;
        this.disabled = false;
        this.data = null;

        this.types = {
            small: "modal-sm",
            def: "",
            large: "modal-lg",
            extraLarge: "modal-xl"
        };
        this.type = this.types.def;

    }

    clear(){
        this.content = null;
        this.header = null;
        this.footer = null;
        this.disabled = false;
        this.data = null;
    }

    open(){
        this.bool = true;

        $("#"+this.id+"_MODAL").modal('show');
    }
    close(){
        $("#"+this.id+"_MODAL").modal('hide');
        this.bool = false;
    }

    setTitle(title){
        /*var tileElement = document.getElementById(this.id+"-title");
        tileElement.innerHTML = title;*/
        this.header = title;
    }
    setFooter(footer){
        /*var tileElement = document.getElementById(this.id+"-title");
        tileElement.innerHTML = title;*/
        this.footer = footer;
    }
    setContent(content){
        /*var conentElement = document.getElementById(this.id+"-body");
        conentElement.innerHTML = content;*/
        this.content = content;
    }
    setType(type){
        this.type = type;
    }

    build(){
        var str = "<div class=\"modal fade text-white\" id=\""+this.id+"_MODAL\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\""+this.id+"\" aria-hidden=\"true\"><div class=\"modal-dialog modal-dialog-scrollable "+this.type+"\" role=\"document\"><div class=\"modal-content bg-dark\">";

        if (this.data != null){
            str = str+"<div id='"+this.id+"_MODAL_DATA' custom-data='"+this.data+"'></div>"
        }

        if (this.header !== null){
            str = str+"<div class=\"modal-header bg-dark\"><h5 class=\"modal-title\" id=\""+this.id+"_TITLE\">"+this.header+"</h5><button type=\"button\" style='color: #fff' class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></div>";
        }
        if (this.content !== null){
            str=str+"<div class=\"modal-body bg-dark\">"+this.content+"</div>";

        }
        if (this.footer !== null){
            str=str+"<div class=\"modal-footer bg-dark\">"+this.footer+"</div>";
        }

        str = str+"</div></div></div>";
        document.getElementById(this.id).innerHTML = str;
    }

    getModal(){
        return document.getElementById(this.id);
    }

    isOpen(){
        return this.bool;
    }
    getData(){
        return getAttribute(this.id+"_MODAL_DATA","custom-data");
    }

    setData(data){
        this.data = data;
    }


}

